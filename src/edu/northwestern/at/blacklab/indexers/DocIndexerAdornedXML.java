package edu.northwestern.at.blacklab.indexers;

//#include "bottom.lic"

import java.io.File;
import java.io.Reader;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import nl.inl.blacklab.index.DocWriter;
import nl.inl.blacklab.index.DocIndexerXmlHandlers;
import nl.inl.blacklab.index.HookableSaxHandler.ContentCapturingHandler;
import nl.inl.blacklab.index.HookableSaxHandler.ElementHandler;
import nl.inl.blacklab.index.Indexer;
import nl.inl.blacklab.index.annotated.AnnotationWriter;
import nl.inl.blacklab.index.annotated.AnnotationWriter.SensitivitySetting;
import nl.inl.blacklab.search.indexmetadata.IndexMetadataWriter;
import nl.inl.blacklab.search.indexmetadata.IndexMetadataImpl;
import nl.inl.util.StringUtil;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.xml.sax.Attributes;

/** Index MorphAdorned TEI files.
 *
 *  <p>Usage:</p>
 *
 *  <code>
 *  java -Xmx4g nl.inl.blacklab.tools.IndexTool operation [options] indexdir [inputfilesdir | deletequery]
 *      edu.northwestern.at.blacklab.indexers.DocIndexerAdornedXML
 *  </code>
 *
 *  <ul>
 *      <li>operation:  create | add | delete</li>
 *      <li>options can include:
 *          <table border="1">
 *              <caption>Options</caption>
 *              <tr><td>--maxdocs <em>n</em></td><td>Stop after indexing <em>n</em> documents.</td></tr>
 *              <tr><td>--indexparam <em>file</em></td><td>Read properties file <em>file</em> with parameters for DocIndexer.<br>
 *                      (NOTE: even without this option, if the current
 *                      directory, the input directory, or the index directory (or their parents)
 *                      contain a file named <em>indexer.properties</em>, the values in that file are passed
 *                      to the indexer.)</td></tr>
 *              <tr><td>---<em>name</em> <em>value</em></td><td>Pass parameter <em>name</em> with
 *                      value <em>value</em> to the indexer.</td></tr>
 *          </table>
 *          </li>
 *      <li>indexdir: output directory name for BlackLab index.</li>
 *      <li>inputfilesdir: input directory containing adorned XML files to index
 *          for the create or add operations.</li>
 *      <li>deletequery specifies a Lucene query to select the files to
 *          delete from the index.</li>
 *  </ul>
 *
 *  <p>Operations:</p>
 *
 *  <ul>
 *  <li>Specify "create" to create a new BlackLab index.  Specifying create
 *      deletes any existing index in the index directory.
 *  </li>
 *
 *  <li>Specify "add" to add or replace files im an existing BlackLab index
 *      from the input directory.
 *  </li>
 *
 *  <li>Specify "delete" to delete files matching the Lucene query
 *      specified by the "deletequery" parameter.
 *  </li>
 *  </ul>
 *
 *  <p>
 *  See the
 *  <a href="http://morphadorner.northwestern.edu">Morphadorner documentation</a>
 *  for details on the TEI format adorned files produced by MorphAdorner.
 *  </p>
 */

public class DocIndexerAdornedXML extends DocIndexerXmlHandlers
{
    /** Version of this indexer. */

    public static final String version  = "2.0.6";

    /** --  Names of the token-level attributes in the xml. -- */
    /*      Not declared static in case we want to allow       */
    /*      configuration of the values in the future.         */

    /** Parts of speech. */

    protected String atrAna = "ana";

    /** cert attribute. */

    protected String atrCert = "cert";

    /** End of sentence flag. */

    protected String atrEOS = "eos";

    /** facs attribute. */

    protected String atrFacs = "facs";

    /** Gap attribute. */

    protected String atrGap = "gap";

    /*  Join. */

    protected String atrJoin = "join";

    /** Lemmata. */

    protected String atrLem = "lem";

    /** Alternate atribute name for lemmata. */

    protected String atrLemma = "lemma";

    /** IOB style named entity. */

    protected String atrNe = "ne";

    /** Orig word attribute. */

    protected String atrOrig = "orig";

    /** Part word flag. */

    protected String atrPart = "part";

    /** Is punctuation flag. */

    protected String atrPc = "pc";

    /** Alternate attribute name for parts of speech. */

    protected String atrPos = "pos";

    /** Standard spelling. */

    protected String atrReg = "reg";

    /** Attribute name for rend. */

    protected String atrRend = "rend";

    /** Attribute name for rendition. */

    protected String atrRendition = "rendition";

    /** Speaker. */

    protected String atrSpeaker = "speaker";

    /** Synonym list. */

    protected String atrSyn = "syn";

    /** Token type. */

    protected String atrType = "type";

    /** Unit marker for end of sentence. */

    protected String atrUnit = "unit";

    /** Element ID. */

    protected String atrWordID = "xml:id";

    /** -- Names of the properties in the lucene index. -- */

    /** TEI div type property name. */

    protected String propDivTypeName = "divtype";

    /** Certainty property name. */

    protected String propCertName = "cert";

    /** End of sentence property name. */

    protected String propEOSName = "eos";

    /** Facs property name. */

    protected String propFacsName = "facs";

    /** Front/middle/back property name. */

    protected String propFMBName = "fmb";

    /** Gap property name. */

    protected String propGapName   = "gap";

    /** In note property name. */

    protected String propInNoteName    = "note";

    /** Orig property name. */

    protected String propOrigName   = "orig";

    /** Page ID property name. */

    protected String propPageIDName = "pageid";

    /** Word ID property name. */

    protected String propWordIDName = "wordid";

    /** Join property name. */

    protected String propJoinName   = "join";

    /** Lemma property name. */

    protected String propLemmaName = "lem";

    /** Named entity property name. */

    protected String propNeName = "ne";

    /** Paratext property name. */

    protected String propParaTextName = "paratext";

    /** Split word part property name. */

    protected String propPartName = "part";

    /** Punctuation flag name. */

    protected String propPcName = "pc";

    /** Part of speech property name. */

    protected String propPosName = "pos";

    /** Standard spelling property name. */

    protected String propRegName = "reg";

    /** Rend property name. */

    protected String propRendName   = "rend";

    /** Rendition property name. */

    protected String propRenditionName  = "rendition";

    /** Reverse standard spelling property name. */

    protected String propRevRegName = "revreg";

    /** Reverse word property name. */

    protected String propRevWordName    = "revword";

    /** Speaker property name. */

    protected String propSpeakerName = "speaker";

    /** Spoken word property name. */

    protected String propSpokenName = "spoken";

    /** Synonym property name. */

    protected String propSynName = "syn";

    /** Token type property name. */

    protected String propTypeName = "type";

    /** Unit type property name. */

    protected String propUnitName = "unit";

    /** Verse property name. */

    protected String propVerseName = "verse";

    /** -- Gap marker characters. */

    protected static String gapCharMarker   = "\u25CF";
    protected static String gapPuncMarker   = "\u25AA";
    protected static String gapSpanMarker   = "\u2026";
    protected static String gapWordMarker   = "\u25CA";

    /** -- Complex field properties for token level attributes. -- */

    /** Correction certainty. */

    protected AnnotationWriter propCert;

    /** Nearest parent div type which contains the token. */

    protected AnnotationWriter propDivType;

    /** End of sentence flag.
     *
     *  "y" means the token ends a sentence.
     *  "n" means the token does not end sentence.
     */

    protected AnnotationWriter propEOS;

    /** Facs setting.
     *
     *  The facs attribute value provides a reference to
     *  an external page image.
     */

    protected AnnotationWriter propFacs;

    /** Front/middle/black flag.
     *
     *  "f" means the word is in the front matter, e.g., in an element
     *  which is a descendant of a &lt;front&gt; tag.
     *
     *  "b" means the word is in the back matter, e.g., in an element
     *  which is a descendant of a &lt;back&gt; tag.
     *
     *  "m" means the word is in the main part of the text, not in the
     *  front or back matter.
     */

    protected AnnotationWriter propFMB;

    /** Gap flag. */

    protected AnnotationWriter propGap;

    /** In note. */

    protected AnnotationWriter propInNote;

    /** Word ID. */

    protected AnnotationWriter propWordID;

    /** Page ID. */

    protected AnnotationWriter propPageID;

    /** Join attribute. */

    protected AnnotationWriter propJoin;

    /** Lemmata. */

    protected AnnotationWriter propLemma;

    /** BlackLab main text field. */

    protected AnnotationWriter propMain;

    /** Named entity tag.
     *
     *  The first token in a named entity takes the form B-xxx
     *  where "xxx" is the type of named entity.  Example: B-PER
     *  starts a person entity.  Subsequent tokens in the entity
     *  take the form I-xxx, e.g., I-PER .  A tag of "O" indicates
     *  the token is not part of a named entity.
     */

    protected AnnotationWriter propNe;

    /** Original spelling attribute. */

    protected AnnotationWriter propOrig;

    /** Main/paratext flag.
     *
     *  "n" means the word is in main text.
     *  "y" means the word in in paratext.
     */

    protected AnnotationWriter propParaText;

    /** Split word flag for words split by decorator tags (e.g., &lt;hi&gt;).
     *
     *  "i" marks the first part of a word.
     *  "m" marks the middle part(s) of a word.
     *  "f" marks the final part of a word.
     *
     *  Non-split words are marked with "n".
     */

    protected AnnotationWriter propPart;

    /** Parts of speech. */

    protected AnnotationWriter propPos;

    /** BlackLab punctuation field. */

    protected AnnotationWriter propPunct;

    /** Punctuation flag.
     *
     *  "y" means the word is punctuation.
     *  "n" means the word is not punctuation.
     */

    protected AnnotationWriter propPc;

    /** Standard spelling. */

    protected AnnotationWriter propReg;

    /** Reverse standard spelling. */

    protected AnnotationWriter propRevReg;

    /** Rend field. */

    protected AnnotationWriter propRend;

    /** Rendition field. */

    protected AnnotationWriter propRendition;

    /** Speaker for a word. */

    protected AnnotationWriter propSpeaker;

    /** Spoken word flag.
     *
     *  "y" means the token is spoken, e.g., is a child of an &lt;sp&gt;,
     *  &lt;said&gt;, or &lt;speaker&gt; tag.
     *
     *  "n" means the token is not spoken.
     */

    protected AnnotationWriter propSpoken;

    /** Synonyms for token. Empty string if no tokens are defined. */

    protected AnnotationWriter propSyn;

    /** Token type.  MorphAdorner uses this field to mark unclear tokens,
     *  e.g., illegible tokens, or machine corrections.
     */

    protected AnnotationWriter propType;

    /** Unit attribute value.  Empty string if token does not have a
     *  unit= value.
     */

    protected AnnotationWriter propUnit;

    /** Verse flag.
     *
     *  A word is in verse if it is a descendant of an &lt;l&gt; tag.
     *
     *  "y" means the word is in verse.
     *  "n" means the word is not in verse.
     */

    protected AnnotationWriter propVerse;

    /** Reverse word spelling. */

    protected AnnotationWriter propRevWord;

    /** Non-paratext tags which can be handled by a generic tag handler.
     *
     *  <p>
     *  Not all of the non-paratext TEI tags are listed here, only those
     *  needed to recognize containment of other tags for information
     *  extraction purposes.
     *  </p>
     */

    protected String[] genericTags  =
        new String[]
        {
            "back" ,
            "biblFull" ,
            "choice" ,
            "creation" ,
            "epHeader" ,
            "fileDesc" ,
            "front" ,
            "keywords" ,
            "monkHeader" ,
            "nuHeader" ,
            "profileDesc" ,
            "publicationStmt" ,
            "said" ,
            "sourceDesc" ,
            "speaker" ,
            "tag" ,
            "text" ,
            "textClass" ,
            "titleStmt"
        };

    /** Tags whose content is paratext.  */

    protected String[] paraTextTags =
        new String[]
        {
            "bibl" ,
            "castGroup" ,
            "castItem" ,
            "castList" ,
            "figDesc" ,
            "figure" ,
            "head" ,
//          "headNote" ,
            "note" ,
            "ref" ,
//          "role" ,
            "roleDesc" ,
            "speaker" ,
            "stage" ,
//          "tailNote" ,
            "trailer"
        };

    /** Inline element tags. */

    protected String[] inlineElementTags    =
        new String[]
        {
            "geogName" ,
            "l" ,
            "name" ,
            "orgName" ,
            "persName" ,
            "placeName" ,
            "rs"
        };

    /** Handlers for TEI elements. */

    protected Map<String , ElementHandler> elementHandlers  =
        new HashMap<String, ElementHandler>();

    /** Word text. */

    protected String wordText   = "";

    /** Original spelling text. */

    protected String origText   = "";

    /** Standard spelling text. */

    protected String regText    = "";

    /** Rend attribute text. */

    protected String rendText   = "";

    /** Rendition attribute text. */

    protected String renditionText  = "";

    /** Document title. */

    protected String title  = "";

    /** Corpus names. */

    protected Set<String> corpora   = new TreeSet<String>();

    /** Document identifiers (ESTC, etc.). */

    protected Set<String> documentIdentifiers   = new TreeSet<String>();

    /** Document genre. */

    protected String genre  = "";

    /** Document subgenre. */

    protected String subgenre   = "";

    /** Final grade. */

    protected String finalGrade = "";

    /** Document authors. */

    protected Set<String> authors   = new TreeSet<String>();

    /** Document keywords. */

    protected Set<String> keywords  = new TreeSet<String>();

    /** Document curators. */

    protected Set<String> curators  = new TreeSet<String>();

    /** Document proofreaders. */

    protected Set<String> proofReaders  = new TreeSet<String>();

    /** Document publication or circulation date. */

    protected int pubDate   = Integer.MIN_VALUE;

    /** Document publication year. */

    protected int publicationYear   = Integer.MIN_VALUE;

    /** Document creation date. */

    protected int creationDate  = Integer.MIN_VALUE;

    /** Word count (count of w elements). */

    protected int wordCount = 0;

    /** Count of pc elements. */

    protected int pcCount   = 0;

    /** Page count (count of pb elements.) */

    protected int pageCount = 0;

    /** Work has page images. */

    protected boolean hasPageImages = false;

    /** True to use epHeader. */

    protected boolean useEpHeader = true;

    /** True to use nuHeader. */

    protected boolean useNuHeader = true;

    /** True to use monkHeader. */

    protected boolean useMonkHeader = true;

    /** True to index cert= value for each word. */

    protected boolean indexCert = false;

    /** True to index div type for each word. */

    protected boolean indexDivType  = false;

    /** True to index facs. */

    protected boolean indexFacs = false;

    /** True to index front/middle/back location of word. */

    protected boolean indexFMB  = false;

    /** True to index gap characters. */

    protected boolean indexGaps = false;

    /** True to index join. */

    protected boolean indexJoin = false;

    /** True to add and index IOB named entity tag for each word. */

    protected boolean indexNE = false;

    /** True to add and index if word appears in a note. */

    protected boolean indexInNote = false;

    /** True to index paratext flag. */

    protected boolean indexParaText = false;

    /** Punctuation indexing type.      */
    /*                                  */
    /*  NONE:     Index no puntuation.    */
    /*  PARTIAL:  Index some punctuation.  */
    /*  ALL:      Index all punctuation.   */

    protected enum IndexPuncType { NONE, PARTIAL, ALL };

    /** How much to index punctuation. */

    protected IndexPuncType indexPunctuation    = IndexPuncType.NONE;

    /** True to index page IDs. */

    protected boolean indexPageIDs   = false;

    /** True to create forward index for page IDs. */

    protected boolean fiPageIDs = false;

    /** True to index word part attribute. */

    protected boolean indexPart = false;

    /** True to index rend. */

    protected boolean indexRend = false;

    /** True to index rendition. */

    protected boolean indexRendition    = false;

    /** True to index reversed words. */

    protected boolean indexReversedWord = false;

    /** True to index reversed standard spellings. */

    protected boolean indexReversedStandardSpelling = false;

    /** True to index synonyms. */

    protected boolean indexSynonyms = false;

    /** True to add and index speaker elements for each word. */

    protected boolean indexSpeaker = false;

    /** True to index if word spoken. */

    protected boolean indexSpoken   = false;

    /** True to index type. */

    protected boolean indexType = false;

    /** True to index unit. */

    protected boolean indexUnit = false;

    /** True to index if word is in verse. */

    protected boolean indexVerse    = false;

    /** Word ID indexing type.             */
    /*                                     */
    /*  NONE:      Do not index word ID.   */
    /*  PARTIAL:   Index portion of word ID following document ID.  */
    /*  FULL:      Index entire word ID.   *
    /*                                     */
    /*  TRUE is a synonym for ALL.         */
    /*  FALSE is a synonym for NONE.       */

    protected enum IndexWordIDsType { NONE, PARTIAL, FULL };

    /** How much to index word IDs. */

    protected IndexWordIDsType indexWordIDs  = IndexWordIDsType.NONE;

    /** True to create forward index for word IDs. */

    protected boolean fiWordIDs = false;

    /** True to mark index for full content viewing allowed. */

    protected boolean allowFullContentViewing   = true;

    /** Use creation date as publication date. */

    protected boolean useCreationDateAsPublicationDate  = false;

    /** Store short file name without directory path. */

    protected boolean storeShortFileName    = true;

    /** Short file name.  Base file name without path or extension. */

    protected String shortFileName  = "";

    /** Unknown part of speech. zz is the NUPos unknown part of speech. */

    protected String unknownPos = "zz";

    /** Set of ignorable div type names. */

    protected Set<String> ignorableDivTypes = new HashSet<String>();

    /** Date patterns for cleaning author field. */

    protected List<Pattern> datePatterns    = new ArrayList<Pattern>();

    /** Date pattern matchers for cleaning author field. */

    protected List<Matcher> dateMatchers    = new ArrayList<Matcher>();

    /** Stack of paratext tag types. */

    protected Deque<Boolean> paraTextStack = new ArrayDeque<Boolean>();

    /** If we're about to process the first file. */

    protected static boolean aboutToProcessFirstFile = true;

    /** Create adorned files indexer.
     *
     *  @param  docWriter   The doc writer.
     *  @param  fileName    The file to index.
     *  @param  reader      The reader.
     */

    @SuppressWarnings("deprecation")
    public DocIndexerAdornedXML
    (
        DocWriter docWriter ,
        final String fileName ,
        Reader reader
    )
    {
        super( docWriter , fileName , reader );

                                //  Display indexer version if first file being processed.

        if ( aboutToProcessFirstFile )
        {
            System.out.println( "CorpusIndexer version " + version );
            aboutToProcessFirstFile = false;
        }
                                //  Check if we're to use epHeader.

        String useEpHeadervalue =
            getParameter( "UseEpHeader" , "" );

        useEpHeader =
            useEpHeadervalue.equalsIgnoreCase( "true" );

                                //  Check if we're to use nuHeader.

        String useNuHeadervalue =
            getParameter( "UseNuHeader" , "" );

        useNuHeader =
            useNuHeadervalue.equalsIgnoreCase( "true" );

                                //  Check if we're to use monkHeader.

        String useMonkHeadervalue   =
            getParameter( "UseMonkHeader" , "" );

        useMonkHeader   =
            useMonkHeadervalue.equalsIgnoreCase( "true" );

                                //  Check if we're to index punctuation.
                                //
                                //  indexPunctuation = "true" or "all":
                                //      index all punctuation
                                //  indexPunctuation = "partial":
                                //      index end-of-sentence punctuation
                                //      with unit="sentence" attribute.
                                //  indexPunctuation = "false" or "none":
                                //      do not index any punctuation.

        String indexPunctuationValue    =
            getParameter( "IndexPunctuation" , "" ).toLowerCase();

        indexPunctuation = IndexPuncType.NONE;

        if ( indexPunctuationValue.equals( "true" ) )
        {
            indexPunctuation    = IndexPuncType.ALL;
        }
        else if ( indexPunctuationValue.equals( "yes" ) )
        {
            indexPunctuation    = IndexPuncType.ALL;
        }
        else if ( indexPunctuationValue.equals( "false" ) )
        {
            indexPunctuation    = IndexPuncType.NONE;
        }
        else if ( indexPunctuationValue.equals( "no" ) )
        {
            indexPunctuation    = IndexPuncType.NONE;
        }
        else if ( indexPunctuationValue.equals( "none" ) )
        {
            indexPunctuation    = IndexPuncType.NONE;
        }
        else if ( indexPunctuationValue.equals( "all" ) )
        {
            indexPunctuation    = IndexPuncType.ALL;
        }
        else if ( indexPunctuationValue.equals( "some" ) )
        {
            indexPunctuation    = IndexPuncType.PARTIAL;
        }
        else if ( indexPunctuationValue.equals( "partial" ) )
        {
            indexPunctuation    = IndexPuncType.PARTIAL;
        }
                                //  Check if we're to index synonyms.

        String indexSynonymsValue   =
            getParameter( "IndexSynonyms" , "" );

        indexSynonyms   =
            indexSynonymsValue.equalsIgnoreCase( "true" );

                                //  Check if we're to index div types
                                //  for each word.
        indexDivType    =
            getParameter( "IndexDivType" , "" ).equalsIgnoreCase( "true" );

                                //  Check if we're indexing gaps.
        indexGaps   =
            getParameter( "IndexGaps" , "" ).equalsIgnoreCase( "true" );

                                //  Check if we're to index page IDs.

        String indexPageIDsValue    =
            getParameter( "IndexPageIDs" , "" );

        indexPageIDs    =
            indexPageIDsValue.equalsIgnoreCase( "true" );

        fiPageIDs   = getParameter( "FIPageIDs" , "" ).equals( "true" );

                                //  Check if we're to index part attribute.

        String partValue    = getParameter( "IndexPart" , "" );

        indexPart   = partValue.equalsIgnoreCase( "true" );

                                //  Check if we're to index word IDs.

        String indexWordIDsValue    =
            getParameter( "IndexWordIDs" , "" );

        if ( indexWordIDsValue.equalsIgnoreCase( "true" ) )
        {
            indexWordIDs = IndexWordIDsType.FULL;
        }
        else if ( indexWordIDsValue.equalsIgnoreCase( "full" ) )
        {
            indexWordIDs = IndexWordIDsType.FULL;
        }
        else if ( indexWordIDsValue.equalsIgnoreCase( "all" ) )
        {
            indexWordIDs = IndexWordIDsType.FULL;
        }
        else if ( indexWordIDsValue.equalsIgnoreCase( "partial" ) )
        {
            indexWordIDs = IndexWordIDsType.PARTIAL;
        }
        else
        {
            indexWordIDs = IndexWordIDsType.NONE;
        }

        fiWordIDs   = getParameter( "FIWordIDs" , "" ).equals( "true" );

                                //  Index rend= attribute.

        String indexRendValue   = getParameter( "IndexRend" , "" );

        indexRend       = indexRendValue.equalsIgnoreCase( "true" );

                                //  Index rendition= attribute.

        String indexRenditionValue  = getParameter( "IndexRendition" , "" );

        indexRendition      = indexRenditionValue.equalsIgnoreCase( "true" );

                                //  Check if we're to store short file
                                //  names without directory path.

        String storeShortFileNameValue  =
            getParameter( "StoreShortFileName" , "" );

        storeShortFileName  =
            storeShortFileNameValue.equalsIgnoreCase( "true" );

                                //  Get the short file name.
                                //  Strip path and extension from input file name.

        shortFileName   = new File( fileName ).getName();

        int periodPos = shortFileName.lastIndexOf( '.' );

        if ( periodPos >= 0 )
        {
            shortFileName   =
                shortFileName.substring( 0 , periodPos );
        }
                                //  Check if we're to mark the index
                                //  to allow the full content of
                                //  the indexed documents to be viewed.

        String allowFullContentViewingValue =
            getParameter( "AllowFullContentViewing" , "" );

        allowFullContentViewing =
            allowFullContentViewingValue.equalsIgnoreCase( "true" );

                                //  Check if we're to index speakers.

        String indexSpeakerValue   =
            getParameter( "IndexSpeaker" , "" );

        indexSpeaker   =
            indexSpeakerValue.equalsIgnoreCase( "true" );

                                //  Check if we're to index IOB named
                                //  entity values (ne= attribute).

        String indexNEValue =
            getParameter( "IndexNE" , "" );

        indexNE = indexNEValue.equalsIgnoreCase( "true" );

                                //  Check if we're to index if word
                                //  is in a note type.

        String indexInNoteValue =
            getParameter( "IndexInNote" , "" );

        indexInNote = indexInNoteValue.equalsIgnoreCase( "true" );

                                //  Check if we're to index the cert attribute.

        String indexCertValue   =
            getParameter( "IndexCert" , "" );

        indexCert   = indexCertValue.equalsIgnoreCase( "true" );

                                //  Check if we're to index the front/middle/back position.

        String indexFMBValue    =
            getParameter( "IndexFMB" , "" );

        indexFMB    = indexFMBValue.equalsIgnoreCase( "true" );

                                //  Check if we're to index in paratext.

        String indexParaTextValue   =
            getParameter( "IndexParaText" , "" );

        indexParaText   = indexParaTextValue.equals( "true" );

                                //  Check if we're to index the reversed word.
        indexReversedWord =
            getParameter( "IndexReversedWord" , "" ).equalsIgnoreCase( "true" );

                                //  Check of we're to index the reversed
                                //  standard spelling.

        indexReversedStandardSpelling   =
            getParameter( "IndexReversedStandardSpelling" , "" ).equals( "true" );

                                //  Check if we're to index unit attribute.
        indexUnit   =
            getParameter( "IndexUnit" , "" ).equalsIgnoreCase( "true" );

                                //  Check if we're to index if word is in verse.
        indexVerse   =
            getParameter( "IndexVerse" , "" ).equalsIgnoreCase( "true" );

                                //  Set the index to allow full
                                //  content viewing if requested.

//      IndexMetadataImpl indexMetadata = (IndexMetadataImpl)docWriter.indexWriter().metadataWriter();
//      indexMetadata.setContentViewable( allowFullContentViewing );
//      IndexMetadataWriter indexMetadataWriter = (IndexMetadataImpl)docWriter.indexWriter().metadataWriter();
//      indexMetadataWriter.setContentViewable( allowFullContentViewing );
//		docWriter.indexWriter().metadataWriter().setContentViewable( allowFullContentViewing );
        IndexMetadataImpl indexMetadata = (IndexMetadataImpl)docWriter.indexWriter().metadata();
        indexMetadata.setContentViewable( allowFullContentViewing );

                                //  Get the unknown part of speech value.

        unknownPos      = getParameter( "UnknownPartOfSpeech" , "zz" );

                                //  Set publication date to creation
                                //  date if provided.

        useCreationDateAsPublicationDate    =
            getParameter( "UseCreationDateAsPublicationDate" , "" ).
                equalsIgnoreCase( "true" );

                                //  Pick up ignorable div types if
                                //  any.  The content of these divs
                                //  will not be indexed.

        String[] ignorableDivTypeNames  =
            getParameter( "IgnorableDivTypes" , "" ).split( " " );

        for ( int i = 0 ; i < ignorableDivTypeNames.length ; i++ )
        {
            ignorableDivTypes.add( ignorableDivTypeNames[ i ] );
        }
                                //  Pick up pattern matchers for
                                //  cleaning author fields.

        for ( int i = 1 ; i < 999999 ; i++ )
        {
            String datePattern  =
                getParameter( "AuthorDatePattern" + i , "" );

            if ( datePattern.length() > 0 )
            {
                Pattern pattern = Pattern.compile( datePattern );
                datePatterns.add( pattern);
                dateMatchers.add( pattern.matcher( "" ) );
            }
            else
            {
                break;
            }
        }
                                //  Create word level fields.

        createWordLevelFields();

                                //  Create handler for the document
                                //  to index.

        DocumentElementHandler documentElementHandler   =
            new DocumentElementHandler()
            {
                @Override
                public void endElement
                (
                    String uri ,
                    String localName ,
                    String qName
                )
                {
                                //  Add collected metadata fields
                                //  to document.

                    addMetaDataFields( fileName );

                    super.endElement( uri , localName , qName );
                }
            };
                                //  Handle root TEI element.

        addHandler( "TEI" , documentElementHandler );

                                //  Handle div element.

        addHandler( "div" , new DivElementHandler() );

                                //  Handle author element.

        addHandler( "author" , new AuthorElementHandler() );

                                //  Handle names in special headers.

//      addHandler( "name" , new NameElementHandler() );

                                //  Handle facsimile element.

        addHandler( "facsimile" , new FacsimileElementHandler() );

                                //  Handle curator element.

        addHandler( "curator" , new CuratorElementHandler() );

                                //  Handle proofreader element.

        addHandler( "proofreader" , new ProofReaderElementHandler() );

                                //  Handle title element.

        addHandler( "title" , new TitleElementHandler() );

                                //  Handle term element.

        addHandler( "term" , new TermElementHandler() );

                                //  Handle pb element.

        addHandler( "pb" , new PbElementHandler() );

                                //  Handle date element.

        addHandler( "date" , new DateElementHandler() );

                                //  Handle publication year element.

        addHandler( "publicationYear" , new PublicationYearElementHandler() );

                                //  Handle circulationYear element.

        addHandler( "circulationYear" , new CirculationYearElementHandler() );

                                //  Handle creationYear element.

        addHandler( "creationYear" , new CreationYearElementHandler() );

                                //  Handle w (word) elements.
                                //  Index these as main contents.

        addHandler( "w" , new WElementHandler() );

                                //  Handle pc (punctuation) elements.
                                //  Optionally index as main contents in
                                //  addition to w elements.

        if ( indexPunctuation == IndexPuncType.ALL )
        {
            addHandler( "pc" , new WElementHandler() );
        }
        else if ( indexPunctuation == IndexPuncType.PARTIAL )
        {
            addHandler( "pc" , new PcElementHandler() );
        }
        else
        {
            addHandler( "pc" , new GenericElementHandler() );
        }
                                //  Handle w/choice/orig element.
                                //  N.B.  This is different from the
                                //  token-level orig= attribute.

        addHandler( "orig" , new OrigElementHandler() );

                                //  Handle w/choice/reg element.

        addHandler( "reg" , new RegElementHandler() );

                                //  Handle sp element.

        addHandler( "sp" , new SpElementHandler() );

                                //  Handle genre element.

        addHandler( "genre" , new GenreElementHandler() );

                                //  Handle subgenre element.

        addHandler( "subgenre" , new SubgenreElementHandler() );

                                //  Handle finalgrade element.

        addHandler( "finalGrade" , new FinalGradeElementHandler() );

                                //  Handle corpus element.

        addHandler( "corpus" , new CorpusElementHandler() );

                                //  Handle document identifier elements.

        addHandler( "tcp" , new DocumentIdentifierElementHandler() );
        addHandler( "estc" , new DocumentIdentifierElementHandler() );
        addHandler( "stc" , new DocumentIdentifierElementHandler() );

                                //  Handle note element types.

        addHandler( "note" , new GenericElementHandler( true , false ) );
        addHandler( "headNote" , new GenericElementHandler( true , false ) );
        addHandler( "tailNote" , new GenericElementHandler( true , false ) );

                                //  Add handlers for any paratext tags not
                                //  already added.

        for ( int i = 0 ; i < paraTextTags.length ; i++ )
        {
            String tag  = paraTextTags[ i ];

            if ( elementHandlers.get( tag ) == null )
            {
                addHandler( tag , new GenericElementHandler( true ) );
            }
        }
                                //  Add handlers for any generic tag not
                                //  already added.

        for ( int i = 0 ; i < genericTags.length ; i++ )
        {
            String tag  = genericTags[ i ];

            if ( elementHandlers.get( tag ) == null )
            {
                addHandler( tag , new GenericElementHandler() );
            }
        }
                                //  Add handlers for inline elements
                                //  not already added.

        for ( int i = 0 ; i < inlineElementTags.length ; i++ )
        {
            String tag  = inlineElementTags[ i ];

            if ( elementHandlers.get( tag ) == null )
            {
                addHandler( tag , new InlineTagHandler() );
            }
        }
    }

    /** Set values for word fields.
     *
     *  @param  attributes      Word element attributes.
     *  @param  isPunctuation   Mark word text as punctuation.
     *
     *  @return     true if word is at end of sentence.
     */

    protected boolean setWordFields( Attributes attributes , boolean isPunctuation )
    {
                                //  Assume word not at end of sentence.

        boolean result  = false;

                                //  Clear word text, standard spelling,
                                //  original spelling,
                                //  and rend/rendition fields.
        wordText        = "";
        origText        = "";
        regText         = "";
        rendText        = "";
        renditionText   = "";

                                //  Mark word as punctuation or not.
        if ( isPunctuation )
        {
            propPc.addValue( "y" );
        }
        else
        {
            propPc.addValue( "n" );
        }
                                //  Check if we have an attribute with
                                //  the name 'unit'.  If so, add it as a
                                //  value for the property 'unit'
                                //  If not, add an empty string as the
                                //  value.

        String unit = attributes.getValue( atrUnit );

        if ( unit != null )
        {
            unit    = unit.toLowerCase();
        }
        else
        {
            unit    = "";
        }

        if ( indexUnit )
        {
            propUnit.addValue( unit );
        }
                                //  Note if unit="sentence".

        result  = unit.equals( "sentence" );

                                //  Optionally index the page ID.
        String id  = "";

        if ( indexPageIDs )
        {
            id = attributes.getValue( atrWordID );

            if ( id == null )
            {
                id  = "";
            }
            else
            {
                int index   = id.lastIndexOf( '-' );

                if ( index != -1 )
                {
                    id  = id.substring( 0 , index );
                }
                else
                {
                    id  = "";
                }
            }

            propPageID.addValue( id );
        }
                                //  Optionally index the word ID.
        id   = "";

        if ( indexWordIDs.equals( IndexWordIDsType.FULL ) )
        {
            id = attributes.getValue( atrWordID );

            if ( id == null )
            {
                id  = "";
            }

            propWordID.addValue( id );
        }
        else if ( indexWordIDs.equals( IndexWordIDsType.PARTIAL ) )
        {
            id = attributes.getValue( atrWordID );

            String savedID  = id;

            if ( id == null )
            {
                id  = "";
            }
            else
            {
                try
                {
                    int index   = id.lastIndexOf( '-' );

                    if ( index != -1 )
                    {
                        id  = id.substring( index + 1 );
                    }
                    else
                    {
                        id  = "";
                    }
                }
                catch ( Exception e )
                {
                    System.out.println
                    (
                        "Bad word id: id=" + savedID +
                        ", shorttFileName=" + shortFileName +
                        ", fixed id=" + id
                    );

                    id  = "";
                }
            }

            propWordID.addValue( id );
        }
                                //  Get join.
        if ( indexJoin )
        {
            String join = attributes.getValue( atrJoin );

            if ( join == null )
            {
                join = "none";
            }

            propJoin.addValue( join );
        }
                               //  Get lemmata.

        String lemma = attributes.getValue( atrLemma );

        if ( lemma == null )
        {
            lemma   = attributes.getValue( atrLem );

            if ( lemma == null )
            {
                lemma = "";
            }
        }

        propLemma.addValue( lemma );

                                //  Get original spelling from orig= attribute.
                                //  If not present, we will set it to the word text
                                //  later.

        origText = attributes.getValue( atrOrig );

                                //  Get parts of speech.

        String pos  = attributes.getValue( atrPos );
        String ana  = attributes.getValue( atrAna );

        if ( pos != null )
        {
            propPos.addValue( pos );
        }
        else
        {
            if ( ana == null )
            {
                ana = unknownPos;
            }

            if ( ana.startsWith( "#" ) )
            {
                ana = ana.substring( 1 );
            }

            propPos.addValue( ana );
        }
                                //  Get standard spelling.
                                //  Delay storing it until we
                                //  know if there is a <reg>
                                //  element descendant of this
                                //  word element containing
                                //  the standard spelling.

        String reg  = attributes.getValue( atrReg );

        if ( reg != null )
        {
            regText = reg;
        }

        if ( indexRend )
        {
            try
            {
                String rend = attributes.getValue( atrRend );

                if ( rend != null )
                {
                    rendText    = rend;
                }

                propRend.addValue( rendText );
            }
            catch ( Exception e )
            {
            //  e.printStackTrace();
            }
        }
                                        //  Get rendition.
        if ( indexRendition )
        {
            try
            {
                String rendition    = attributes.getValue( atrRendition );

                if ( rendition != null )
                {
                    renditionText   = rendition;
                }
                else
                {
                    renditionText   = "";
                }

                propRendition.addValue( renditionText );
            }
            catch ( Exception e2 )
            {
                //  e2.printStackTrace();
            }
        }
                                //  Get type.
        if ( indexType )
        {
            String typeAttr = attributes.getValue( atrType );

            if ( typeAttr == null )
            {
                typeAttr    = "";
            }

            propType.addValue( typeAttr );
        }
                                //  Get cert.
        if ( indexCert )
        {
            String certAttr = attributes.getValue( atrCert );

            if ( certAttr == null )
            {
                certAttr    = "";
            }

            propCert.addValue( certAttr );
        }
                                //  Get named entity BIO tag if present.
        if ( indexNE )
        {
            String ne   = attributes.getValue( atrNe );

            if ( ne == null )
            {
                ne  = "O";
            }

            propNe.addValue( ne );
        }
                                //  Get split word flag.
                                //
                                //  "i" marks the first part of a word.
                                //  "m" marks the middle part(s) of a word.
                                //  "f" marks the final part of a word.
                                //
                                //  Non-split words are marked with "n".
        if ( indexPart )
        {
            String part = attributes.getValue( atrPart );

            if ( part == null )
            {
                part    = "n";
            }

            propPart.addValue( part.toLowerCase() );
        }
                                //  Get verse flag.
                                //
                                //  A word is in verse if it is a
                                //  descendant of an <l> tag.
                                //
                                //  "y" means the word is in verse.
                                //  "n" means the word is not in verse.
        if ( indexVerse )
        {
            String inVerse  =
                elementHandlers.get( "l" ).insideElement() ? "y" : "n";

            propVerse.addValue( inVerse );
        }
                               //  Get note flag.
                                //
                                //  A word is in a note if it is a
                                //  descendant of a <note>, <headNote>,
                                //  or <tailNote> tag.
                                //
                                //  "y" means the word is in a note.
                                //  "n" means the word is not in a note.
        if ( indexInNote )
        {
            boolean inNote  =
                elementHandlers.get( "note" ).insideElement() ||
                elementHandlers.get( "headNote" ).insideElement() ||
                elementHandlers.get( "tailNote" ).insideElement();

            String inNoteVal    = inNote ? "y" : "n";

            try
            {
                propInNote.addValue( inNoteVal );
            }
            catch ( Exception e )
            {
                System.out.println
                (
                    "propInNote.addValue failed for " + inNoteVal
                );
            }

            if ( inNoteVal.equals( "y" ) )
            {
                System.out.println( "indexInNote:  lemma=" + lemma +
                    ", inNoteVal=" + inNoteVal );
            }
        }
                                //  A flag indicating if the word ends
                                //  a sentence.  "y" means the word
                                //  ends a sentence, "n" means it does
                                //  not end a sentence.
                                //
                                //  Check if we have an attribute with
                                //  the name 'eos'.  If so, map a value
                                //  of 1 to "y" and a value of "0" to "n".
                                //  If the value does not exist, check if
                                //  we had a unit="sentence" and add
                                //  eos="y".  Otherwise, add "n" as the
                                //  value.

        String eos      = "n";
        String eosAttr  = attributes.getValue( atrEOS );

        if ( eosAttr != null )
        {
            if ( eosAttr.trim().equals( "1" ) )
            {
                eos = "y";
            }
        }
        else if ( unit.equals( "sentence" ) )
        {
            eos = "y";
        }

        propEOS.addValue( eos );

                                //  Note if eos is "y".

        result  = result || eos.equals( "y" );

                                //  Add front/middle/black flag.
                                //
                                //  "f" means the word is in the
                                //  front matter, e.g., in an element
                                //  which is a child of a <front> tag.
                                //
                                //  "b" means the word is in the
                                //  back matter, e.g., in an element
                                //  which is a child of a <back> tag.
                                //
                                //  "m" means the word is in the
                                //  main part of the text, not in the
                                //  front or back matter.
        if ( indexFMB )
        {
            String isFMB    = "m";

            if ( elementHandlers.get( "front" ).insideElement() )
            {
                isFMB   = "f";
            }
            else if ( elementHandlers.get( "back" ).insideElement() )
            {
                isFMB   = "b";
            }

            propFMB.addValue( isFMB );
        }
                                //  Add main/paratext flag.
                                //  "n" means the word is in main text.
                                //  "y" means the word in in paratext.
        if ( indexParaText )
        {
            String isParaText   = ( ( paraTextStack.size() > 0 ) ? "y" : "n" );

            propParaText.addValue( isParaText );
        }
                                //  Add spoken word flag.
                                //
                                //  "y" means the word is spoken, e.g.,
                                //  is a child of an <sp>, <said>, or
                                //  <speaker> tag.
                                //
                                //  "n" means the word is not spoken.
        if ( indexSpoken )
        {
            String isSpoken =
                (
                    (   elementHandlers.get( "sp" ).insideElement() ||
                        elementHandlers.get( "said" ).insideElement()
                    ) && elementHandlers.get( "speaker" ).insideElement()
                ) ? "y" : "n";

            propSpoken.addValue( isSpoken );
        }
                                //  Add and index speaker element for
                                //  this word, if requested.
        if ( indexSpeaker )
        {
            ElementHandler spElementHandler = elementHandlers.get( "sp" );

            String speaker  = "";

            if ( spElementHandler != null )
            {
                speaker = ((SpElementHandler)spElementHandler).getSpeaker();
            }

            propSpeaker.addValue( speaker );
        }
                                //  Add div type.
        if ( indexDivType )
        {
            ElementHandler divElementHandler    =
                elementHandlers.get( "div" );

            String divType  = "";

            if ( divElementHandler != null )
            {
                divType = ( ( DivElementHandler )divElementHandler ).getDivType();
            }

            propDivType.addValue( divType );
        }
                                //  Add facs.
        if ( indexFacs )
        {
            String facs = "";

            if ( attributes.getValue( atrFacs ) != null )
            {
                                //  Use word-level facs if present.

                facs    = attributes.getValue( atrFacs );
            }
            else
            {
                                //  Else try page break level facs.

                ElementHandler pbElementHandler = elementHandlers.get( "pb" );

                if ( pbElementHandler != null )
                {
                    facs    = ( ( PbElementHandler )pbElementHandler ).getFacs();
                }
            }

            propFacs.addValue( facs );
        }
                                //  Add synonyms.  The syn= attribute
                                //  specifies a whitespace separated
                                //  list of synonyms for the token.
        if ( indexSynonyms )
        {
            String syn  = "";

            if ( attributes.getValue( atrSyn ) != null )
            {
                syn = attributes.getValue( atrSyn );

                String[] synonyms   =  syn.split( "\\s+" );

                int nSyn    = synonyms.length;

                for ( int k = 0 ; k < nSyn ; k++ )
                {
                    propSyn.addValue( synonyms[ k ] , ( k > 0 ) ? 0 : 1 );
                }
            }
            else
            {
                propSyn.addValue( syn );
            }
        }

        return result;
    }

    /** Create word level fields for indexing. */

    protected void createWordLevelFields()
    {
                                //  Main text.

        propMain    = mainAnnotation();

                                //  Punctuation text.

        propPunct   = punctAnnotation();

                                //  Is punctuation flag.
        propPc      =
            addAnnotation
            (
                propPcName ,
                SensitivitySetting.ONLY_INSENSITIVE
            );

        propPc.setHasForwardIndex( true );

                                //  Cert.
        if ( indexCert )
        {
            propCert    =
               addAnnotation
                (
                    propCertName ,
                    SensitivitySetting.ONLY_INSENSITIVE
                );

            propCert.setHasForwardIndex( true );
        }
                                //  Div type.
        if ( indexDivType )
        {
            propDivType     =
                addAnnotation
                (
                    propDivTypeName ,
                    SensitivitySetting.ONLY_INSENSITIVE
                );

            propDivType.setHasForwardIndex( true );
         }
                                //  End of sentence flag.
        propEOS     =
            addAnnotation
            (
                propEOSName ,
                SensitivitySetting.ONLY_INSENSITIVE
            );

        propEOS.setHasForwardIndex( true );

                                // Facs identifier.
        if ( indexFacs )
        {
            propFacs        =
               addAnnotation
                (
                    propFacsName ,
                    SensitivitySetting.ONLY_INSENSITIVE
                );

           propFacs.setHasForwardIndex( true );
        }
                                //  Front/middle/back matter flag.
        if ( indexFMB )
        {
            propFMB     =
               addAnnotation
                (
                    propFMBName ,
                    SensitivitySetting.ONLY_INSENSITIVE
                );

            propFMB.setHasForwardIndex( true );
        }
                                //  Gap.
        propGap    =
            addAnnotation
            (
                propGapName ,
                SensitivitySetting.ONLY_INSENSITIVE
            );

        propGap.setHasForwardIndex( true );

                                //  Join.
        if ( indexJoin )
        {
            propJoin    =
               addAnnotation
                (
                    propJoinName ,
                    SensitivitySetting.ONLY_INSENSITIVE
                );

           propJoin.setHasForwardIndex( false );
        }
                                //  Lemmata.
        propLemma   =
            addAnnotation
            (
                propLemmaName ,
                SensitivitySetting.CASE_AND_DIACRITICS_SEPARATE
            );

        propLemma.setHasForwardIndex( true );

                                //  IOB Named entity tag.
        if ( indexNE )
        {
            propNe  =
                addAnnotation
                (
                    propNeName ,
                    SensitivitySetting.ONLY_INSENSITIVE
                );

            propNe.setHasForwardIndex( true );
        }
                                //  In note flag.
        if ( indexInNote )
        {
            propInNote   =
                addAnnotation
                (
                    propInNoteName ,
                    SensitivitySetting.ONLY_INSENSITIVE
                );

            propInNote.setHasForwardIndex( true );
        }
                               //  Original spelling.
        propOrig    =
            addAnnotation
            (
                propOrigName ,
                SensitivitySetting.CASE_AND_DIACRITICS_SEPARATE
            );

        propOrig.setHasForwardIndex( true );

                                //  Paratext flag.
        if ( indexParaText )
        {
            propParaText    =
               addAnnotation
                (
                    propParaTextName ,
                    SensitivitySetting.ONLY_INSENSITIVE
                );

            propParaText.setHasForwardIndex( true );
        }
                                //  Word part flag.
        if ( indexPart )
        {
            propPart    =
               addAnnotation
                (
                    propPartName ,
                    SensitivitySetting.ONLY_INSENSITIVE
                );

            propPart.setHasForwardIndex( false );
        }
                                //  Parts of speech.
        propPos     =
            addAnnotation
            (
                propPosName ,
                SensitivitySetting.ONLY_INSENSITIVE
            );

        propPos.setHasForwardIndex( true );

                                //  Standard spelling.
        propReg     =
            addAnnotation
            (
                propRegName ,
                SensitivitySetting.CASE_AND_DIACRITICS_SEPARATE
            );

        propReg.setHasForwardIndex( true );

                                //  Reverse standard spelling.

        if ( indexReversedStandardSpelling )
        {
            propRevReg     =
                addAnnotation
                (
                    propRevRegName ,
                    SensitivitySetting.CASE_AND_DIACRITICS_SEPARATE
                );

            propRevReg.setHasForwardIndex( true );
        }
                                //  Rend.
        if ( indexRend )
        {
            propRend    =
               addAnnotation
                (
                    propRendName ,
                    SensitivitySetting.CASE_AND_DIACRITICS_SEPARATE
                );

            propRend.setHasForwardIndex( false );
        }
                                //  Rendition.
        if ( indexRendition )
        {
            propRendition   =
               addAnnotation
                (
                    propRenditionName ,
                    SensitivitySetting.CASE_AND_DIACRITICS_SEPARATE
                );

            propRendition.setHasForwardIndex( false );
        }
                                //  Speaker.
        if ( indexSpeaker )
        {
            propSpeaker =
               addAnnotation
                (
                    propSpeakerName ,
                    SensitivitySetting.ONLY_INSENSITIVE
                );

            propSpeaker.setHasForwardIndex( true );
        }
                                //  Spoken word flag.
        if ( indexSpoken )
        {
            propSpoken      =
                addAnnotation
                (
                    propSpokenName ,
                    SensitivitySetting.CASE_AND_DIACRITICS_SEPARATE
                );

            propSpoken.setHasForwardIndex( true );
        }
                                //  Synonyms.
        if ( indexSynonyms )
        {
            propSyn =
                addAnnotation
                (
                    propSynName ,
                    SensitivitySetting.CASE_AND_DIACRITICS_SEPARATE
                );

            propParaText.setHasForwardIndex( true );
        }
                                //  Type.
        if ( indexType )
        {
            propType    =
               addAnnotation
                (
                    propTypeName ,
                    SensitivitySetting.ONLY_INSENSITIVE
                );

            propType.setHasForwardIndex( false );
        }
                                //  Unit type.
        if ( indexUnit )
        {
            propUnit    =
               addAnnotation
                (
                    propUnitName ,
                    SensitivitySetting.ONLY_INSENSITIVE
                );

            propUnit.setHasForwardIndex( false );
        }
                                //  Verse flag.
        if ( indexVerse )
        {
            propVerse   =
               addAnnotation
                (
                    propVerseName ,
                    SensitivitySetting.ONLY_INSENSITIVE
                );

            propVerse.setHasForwardIndex( true );
        }
                                //  Reverse word.

        if ( indexReversedWord )
        {
            propRevWord     =
                addAnnotation
                (
                    propRevWordName ,
                    SensitivitySetting.CASE_AND_DIACRITICS_SEPARATE
                );

            propRevWord.setHasForwardIndex( true );
        }
                                //  Page ID.
        if ( indexPageIDs )
        {
            propPageID   =
               addAnnotation
                (
                    propPageIDName ,
                    SensitivitySetting.ONLY_INSENSITIVE
                );

            propPageID.setHasForwardIndex( fiPageIDs );
        }
                                  //  Word ID.

        if ( indexWordIDs != IndexWordIDsType.NONE )
        {
            propWordID   =
               addAnnotation
                (
                    propWordIDName ,
                    SensitivitySetting.ONLY_INSENSITIVE
                );

            propWordID.setHasForwardIndex( fiWordIDs );
        }
                                //  Register contents field.

        registerContentsField();
    }

    /** Add a metadata field.
     *
     *  @param  fieldName   The metadata field name.
     *  @param  fieldValue  The metadata field value.
     *
     */

    protected void addAMetadataField( String fieldName , String fieldValue )
    {
        addMetadataField( fieldName , fieldValue );
    }

    /** Add metadata fields for a document.
     *
     *  @param  fileName    The name of the file being indexed.
     *
     *  <p>
     *  Metadata fields include authors, title, keywords, publication
     *  date, word count, page count.
     *  </p>
     */

    protected void addMetaDataFields( String fileName )
    {
                                //  Add page and word counts.

        addAMetadataField( "wordcount" , Integer.toString( wordCount ) );
        addAMetadataField( "pagecount" , Integer.toString( pageCount ) );
        addAMetadataField( "pcCount" , Integer.toString( pcCount ) );

                                //  Add the authors.

        StringBuilder sb    = new StringBuilder();

        for ( String author : authors )
        {
            if ( sb.length() > 0 )
            {
                sb.append( "; " );
            }

            sb.append( author );
        }

        addAMetadataField( "author" , sb.toString().trim() );

                                //  Add the title.

        addAMetadataField( "title" , title );

                                //  Add the short file name.

        if ( storeShortFileName )
        {
            addAMetadataField( "filename" , shortFileName );
        }
                                //  Add the keywords.
        sb.setLength( 0 );

        for ( String keyword : keywords )
        {
            if ( sb.length() > 0 )
            {
                sb.append( "; " );
            }

            sb.append( keyword );
        }

        addAMetadataField( "keywords" , sb.toString().trim() );

                                //  Add the creation date.

        String creationDateValue    = "";

        if ( creationDate != Integer.MIN_VALUE )
        {
            creationDateValue   = Integer.toString( creationDate );
        }

        addAMetadataField( "creationdate" , creationDateValue );

                                //  Add the publication date.

        String pubDateValue = "";

                                //  Set publication date to creation
                                //  date if requested.

        if ( useCreationDateAsPublicationDate )
        {
            pubDate = creationDate;
        }

        if ( pubDate != Integer.MIN_VALUE )
        {
            pubDateValue    = Integer.toString( pubDate );
        }
        else if ( publicationYear != Integer.MIN_VALUE )
        {
            pubDateValue    = Integer.toString( publicationYear );
        }

        addAMetadataField( "pubdate" , pubDateValue );

                                //  Add epHeader/nuHeader/monkHeader values.

        if ( useEpHeader || useMonkHeader || useNuHeader )
        {
            addAMetadataField( "genre" , genre );
            addAMetadataField( "subgenre" , subgenre );
            addAMetadataField( "finalgrade" , finalGrade );

            sb.setLength( 0 );

            for ( String corpus : corpora )
            {
                if ( sb.length() > 0 )
                {
                    sb.append( "; " );
                }

                sb.append( corpus );
            }

            addAMetadataField( "corpora" , sb.toString() );

            sb.setLength( 0 );

            for ( String identifier : documentIdentifiers )
            {
                if ( sb.length() > 0 )
                {
                    sb.append( "; " );
                }

                sb.append( identifier );
            }

            addAMetadataField( "identifier" , sb.toString() );

            sb.setLength( 0 );

            for ( String proofReader : proofReaders )
            {
                if ( sb.length() > 0 )
                {
                    sb.append( "; " );
                }

                sb.append( proofReader);
            }

            addAMetadataField( "proofreader" , sb.toString() );

            sb.setLength( 0 );

            for ( String curator : curators )
            {
                if ( sb.length() > 0 )
                {
                    sb.append( "; " );
                }

                sb.append( curator );
            }

            addAMetadataField( "curator" , sb.toString() );

            if ( hasPageImages )
            {
                addAMetadataField( "haspageimages" , "y" );
            }
            else
            {
                addAMetadataField( "haspageimages" , "n" );
            }
        }
    }

    /** Add a handler for an element.
     *
     *  @param  condition   Element name.
     *  @param  handler     The element handler.
     *
     *  @return             The element handler.
     */

    public ElementHandler addHandler
    (
        String condition ,
        ElementHandler handler
    )
    {
        elementHandlers.put( condition , handler );

        return super.addHandler( condition , handler );
    }

    /** Generic element handler.
     *
     *  <p>
     *  Optionally consumes the character content of the tag and normalizes
     *  the whitespace.  Used to hold a place on the tag stack
     *  so that descendant tags can determine if they are descendants
     *  of a specific tag type or not.
     *  </p>
     */

    public class GenericElementHandler extends ElementHandler
    {
        /** ParaText tag flag. */

        protected boolean isParaTextTag = false;

        /** Consume text flag. */

        protected boolean consumeText = false;

        /** Create generic element handler. */

        public GenericElementHandler()
        {
            this.isParaTextTag  = false;
        }

        /** Create generic element handler.
         *
         *  @param  isParaTextTag   True if paratext tag.
         */

        public GenericElementHandler
        (
            boolean isParaTextTag
        )
        {
            this.isParaTextTag  = isParaTextTag;
            this.consumeText    = true;
        }

        /** Create generic element handler.
         *
         *  @param  isParaTextTag   True if paratext tag.
         *  @param  consumeText     True to consume text.
         */

        public GenericElementHandler
        (
            boolean isParaTextTag ,
            boolean consumeText
        )
        {
            this.isParaTextTag  = isParaTextTag;
            this.consumeText    = consumeText;
        }

        @Override
        public void startElement
        (
            String uri ,
            String localName ,
            String qName ,
            Attributes attributes
        )
        {
                                //  If this a paratext tag,
                                //  add it to the paratext stack.

            if ( isParaTextTag )
            {
                paraTextStack.push( isParaTextTag );
            }
                                //  Consume text if requested.

            if ( consumeText )
            {
                consumeCharacterContent();
            }
        }

        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
            super.endElement( uri , localName , qName );

                                //  Pop the paratext stack if this
                                //  was a paratext tag.

            if ( isParaTextTag )
            {
                paraTextStack.pop();
            }
        }
    }

    /** Generic element handler.
     *
     *  <p>
     *  Optionally consumes the character content of the tag and normalizes
     *  the whitespace.  Used to hold a place on the tag stack
     *  so that descendant tags can determine if they are descendants
     *  of a specific tag type or not.
     *  </p>
     */

    public class GenericElementHandlerOld extends ElementHandler
    {
        /** ParaText tag flag. */

        protected boolean isParaTextTag = false;

        /** Consume text flag. */

        protected boolean consumeText = false;

        /** Create generic element handler. */

        public GenericElementHandlerOld()
        {
            this.isParaTextTag  = false;
        }

        /** Create generic element handler.
         *
         *  @param  isParaTextTag   True if paratext tag.
         */

        public GenericElementHandlerOld
        (
            boolean isParaTextTag
        )
        {
            this.isParaTextTag  = isParaTextTag;
            this.consumeText    = true;
        }

        /** Create generic element handler.
         *
         *  @param  isParaTextTag   True if paratext tag.
         *  @param  consumeText     True to consume text.
         */

        public GenericElementHandlerOld
        (
            boolean isParaTextTag ,
            boolean consumeText
        )
        {
            this.isParaTextTag  = isParaTextTag;
            this.consumeText    = consumeText;
        }

        @Override
        public void startElement
        (
            String uri ,
            String localName ,
            String qName ,
            Attributes attributes
        )
        {
                                //  If this a paratext tag,
                                //  add it to the paratext stack.

            if ( isParaTextTag )
            {
                paraTextStack.push( isParaTextTag );
            }
                                //  Consume text if requested.

            if ( consumeText )
            {
                consumeCharacterContent();
            }
        }

        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
            super.endElement( uri , localName , qName );

                                //  Pop the paratext stack if this
                                //  was a paratext tag.

            if ( isParaTextTag )
            {
                paraTextStack.pop();
            }
        }
    }

    /** A body element. */

    public ElementHandler body  =
        addHandler( "body" , new ElementHandler()
        {
            @Override
            public void startElement
            (
                String uri ,
                String localName ,
                String qName ,
                Attributes attributes
        )
        {
            // Clear it to capture punctuation and words.

            consumeCharacterContent();
        }

        @Override
        public void endElement( String uri , String localName , String qName )
        {
            // Before ending the document, add the final bit of punctuation.

            propPunct.addValue
            (
                StringUtil.normalizeWhitespace( consumeCharacterContent() )
            );

            super.endElement( uri , localName , qName );
        }
    });

    /** A div element. */

    public class DivElementHandler extends GenericElementHandler
    {
        /** Stack of div types. */

        protected Deque<String> divTypeStack    = new ArrayDeque<String>();

        @Override
        public void startElement
        (
            String uri ,
            String localName ,
            String qName ,
            Attributes attributes
        )
        {
            super.startElement( uri , localName , qName , attributes );

                                //  Extract div type if given.
                                //  Set div type to empty string otherwise.

            String divType  = "";

            if ( attributes.getValue( "type" ) != null )
            {
                divType = attributes.getValue( "type" );
            }
                                //  Push div type onto div type stack.

            divTypeStack.push( divType );
        }

        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
            super.endElement( uri , localName , qName );

                                //  Pop the div type stack.

            divTypeStack.pop();
        }

        /** Get current div type.
         *
         *  @return     The current div type.
         */

        public String getDivType()
        {
            String result   = "";

            if ( divTypeStack.size() > 0 )
            {
                result  = divTypeStack.peek();
            }

            return result;
        }

        /** Should contents of current div be indexed?
         *
         *  @return     True to index contents of current div.
         */

        public boolean indexDivContents()
        {
            String divType  = "";

            if ( divTypeStack.size() > 0 )
            {
                divType = divTypeStack.peek();
            }

            return !ignorableDivTypes.contains( divType );
        }
    }

    /** A facsimile element. */

    public class FacsimileElementHandler extends GenericElementHandler
    {
        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
                                //  Text has page images.

            boolean hasPageImages    = true;

                                //  Ignore the facsimile definitions.

            consumeCharacterContent();

            super.endElement( uri , localName , qName );
        }
    }

    /** A genre element. */

    public class GenreElementHandler extends GenericElementHandler
    {
        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
            boolean inMonkHeader    =
                useMonkHeader &&
                elementHandlers.get( "monkHeader" ).insideElement();

            boolean inNuHeader      =
                useNuHeader &&
                elementHandlers.get( "nuHeader" ).insideElement();

            boolean inEpHeader      =
                useEpHeader &&
                elementHandlers.get( "epHeader" ).insideElement();

            if ( !( inMonkHeader || inEpHeader || inNuHeader ) )
            {
                super.endElement( uri , localName , qName );
                return;
            }

            genre   = consumeCharacterContent();
            genre   = genre.replaceAll( "\\s+" , " " ).trim();

            super.endElement( uri , localName , qName );
        }
    }

    /** A subgenre element. */

    public class SubgenreElementHandler extends GenericElementHandler
    {
        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
            boolean inEpHeader  =
                useEpHeader &&
                elementHandlers.get( "epHeader" ).insideElement();

            boolean inMonkHeader    =
                useMonkHeader &&
                elementHandlers.get( "monkHeader" ).insideElement();

            boolean inNuHeader      =
                useNuHeader &&
                elementHandlers.get( "nuHeader" ).insideElement();

            if ( !( inEpHeader || inMonkHeader || inNuHeader ) )
            {
                super.endElement( uri , localName , qName );
                return;
            }

            subgenre    = consumeCharacterContent();
            subgenre    = subgenre.replaceAll( "\\s+" , " " ).trim();

            super.endElement( uri , localName , qName );
        }
    }

    /** A finalGrade element. */

    public class FinalGradeElementHandler extends GenericElementHandler
    {
        @Override
        public void startElement
        (
            String uri ,
            String localName ,
            String qName ,
            Attributes attributes
        )
        {
            super.startElement( uri , localName , qName , attributes );

            consumeCharacterContent();
        }

        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
            boolean inEpHeader      =
                useEpHeader &&
                elementHandlers.get( "epHeader" ).insideElement();

            boolean inNuHeader      =
                useNuHeader &&
                elementHandlers.get( "nuHeader" ).insideElement();

            if ( !( inEpHeader || inNuHeader ) )
            {
                super.endElement( uri , localName , qName );
                return;
            }

            finalGrade  = consumeCharacterContent();

            finalGrade  =
                finalGrade.replaceAll( "\\s+" , " " ).trim();

            super.endElement( uri , localName , qName );
        }
    }

    /** A paragraph break. */

    public class PbElementHandler extends GenericElementHandler
    {
        /** Last encountered facs= value. */

        protected String facs   = "";

        @Override
        public void startElement
        (
            String uri ,
            String localName ,
            String qName ,
            Attributes attributes
        )
        {
                                //  Extract facs attribute if given.
                                //  Set facs to empty string otherwise.
            facs    = "";

            if ( attributes.getValue( "facs" ) != null )
            {
                facs    = attributes.getValue( "facs" );
            }

                                //  Increment count of <pb> elements.
            pageCount++;
        }

        /** Get current facs= value.
         *
         *  @return     The current facs= value.
         */

        public String getFacs()
        {
            return facs;
        }
    }

    /** A speech. */

    public class SpElementHandler extends GenericElementHandler
    {
        /** Stack of speakers. */

        protected Deque<String> speakerStack    = new ArrayDeque<String>();

        @Override
        public void startElement
        (
            String uri ,
            String localName ,
            String qName ,
            Attributes attributes
        )
        {
            super.startElement( uri , localName , qName , attributes );

                                //  Extract speaker ID from who= attribute.
                                //  Set speaker to empty string if who=
                                //  is missing.

            String speaker  = "";

            if ( attributes.getValue( "who" ) != null )
            {
                speaker = attributes.getValue( "who" );
            }
                                //  Push speaker onto speaker stack.

            speakerStack.push( speaker );
        }

        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
            super.endElement( uri , localName , qName );

                                //  Pop the speaker stack.

            speakerStack.pop();
        }

        /** Get current speaker.
         *
         *  @return     The current speaker.
         */

        public String getSpeaker()
        {
            String result   = "";

            if ( speakerStack.size() > 0 )
            {
                result  = speakerStack.peek();
            }

            return result;
        }
    }

    /** An author element. */

    public class AuthorElementHandler extends GenericElementHandler
    {
        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
            boolean inEpHeader  =
                useEpHeader &&
                elementHandlers.get( "epHeader" ).insideElement();

            boolean inNuHeader      =
                useNuHeader &&
                elementHandlers.get( "nuHeader" ).insideElement();

            if ( inEpHeader || inNuHeader )
            {
                String authorString =
                    cleanAuthorString( consumeCharacterContent() );

                if ( authorString.length() > 0 )
                {
                    authors.add( authorString );
                }
            }
                                //  Try sourceDesc first, then
                                //  fileDesc.

            if ( !useNuHeader && !useEpHeader )
            {
                if  (   !elementHandlers.get( "sourceDesc" ).insideElement() ||
                        !elementHandlers.get( "biblFull" ).insideElement() ||
                        !elementHandlers.get( "titleStmt" ).insideElement()
                    )
                {
                    if  (   !elementHandlers.get( "fileDesc" ).insideElement() ||
                            !elementHandlers.get( "titleStmt" ).insideElement()
                        )
                    {
                        super.endElement( uri , localName , qName );
                        return;
                    }
                }
                                //  Get author name.

                String authorString =
                    cleanAuthorString( consumeCharacterContent() );

                if ( authorString.length() > 0 )
                {
                    authors.add( authorString );
                }
            }

            super.endElement( uri , localName , qName );
        }
    }

    /** An author or curator name element in a special header. */

    public class NameElementHandler extends GenericElementHandler
    {
        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
                                //  See if we are in a special header,
                                //  and we are using the values in that
                                //  header.
            boolean inEpHeader  =
                useEpHeader &&
                elementHandlers.get( "epHeader" ).insideElement();

            boolean inNuHeader      =
                useNuHeader &&
                elementHandlers.get( "nuHeader" ).insideElement();

            boolean inMonkHeader        =
                useMonkHeader &&
                elementHandlers.get( "monkHeader" ).insideElement();

                                //  Not in header or not using header?
                                //  Do nothing.

            if ( !( inEpHeader || inNuHeader || inMonkHeader ) )
            {
                super.endElement( uri , localName , qName );
                return;
            }
                                //  Get name text.

            String nameString   = consumeCharacterContent();

            nameString  = nameString.replaceAll( "\\s+" , " " ).trim();

            nameString  = cleanPersonString( nameString );

                                //  In a special header.
                                //  See if the parent of tne <name>
                                //  element is an author, curator,
                                //  or proofreader element and add name
                                //  to the appropriate collection.

            if ( elementHandlers.get( "author" ).insideElement() )
            {
                if ( nameString.length() > 0 )
                {
                    authors.add( nameString );
                }
            }
            else if ( elementHandlers.get( "curator" ).insideElement() )
            {
                if ( nameString.length() > 0 )
                {
                    curators.add( nameString );
                }
            }
            else if ( elementHandlers.get( "proofreader" ).insideElement() )
            {
                if ( nameString.length() > 0 )
                {
                    proofReaders.add( nameString );
                }
            }

            super.endElement( uri , localName , qName );
        }
    }

    /** Clean author string.
     *
     *  @param  authorString    The author string to clean.
     *
     *  @return                 Author string cleaned of dates and
     *                          other cruft.
     *
     *  <p>
     *  The crufty cleaning is not as complete as we'd like.
     *  Ideally at tome point in the future the author field
     *  can be consistently formatted in the input texts.
     *  </p>
     */

    public String cleanAuthorString( String authorString )
    {
                                //  Normalize whitespace.

        String result   = authorString.replaceAll( "\\s+" , " " ).trim();

                                //  Remove dates and other cruft from
                                //  the author string.

        for ( int i = 0 ; i < dateMatchers.size() ; i++ )
        {
            while ( dateMatchers.get( i ).reset( result ).matches() )
            {
                result  = dateMatchers.get( i ).group( 1 ).trim();
            }
        }

        return cleanPersonString( result );
    }

    /** A title element. */

    public class TitleElementHandler extends GenericElementHandler
    {
        @Override
        public void startElement
        (
            String uri ,
            String localName ,
            String qName ,
            Attributes attributes
        )
        {
            super.startElement( uri , localName , qName , attributes );
        }

        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
                                    //  If title not set yet ...

            if ( title.length() == 0 )
            {
                                    //  Pick up title from either
                                    //  monkHeader, sourceDesc, or
                                    //  fileDesc.

                if  (   ( elementHandlers.get( "monkHeader" ).insideElement() ||
                          elementHandlers.get( "epHeader" ).insideElement() ||
                          elementHandlers.get( "nuHeader" ).insideElement()
                        ) ||
                        (   elementHandlers.get( "sourceDesc" ).insideElement() &&
                            elementHandlers.get( "biblFull" ).insideElement() &&
                            elementHandlers.get( "titleStmt" ).insideElement()
                        ) ||
                        (   elementHandlers.get( "fileDesc" ).insideElement() &&
                            elementHandlers.get( "titleStmt" ).insideElement()
                        )
                    )
                {
                    String titleString  = consumeCharacterContent();

                    titleString =
                        titleString.replaceAll( "\\s+" , " " ).trim();

                    if ( titleString.length() > 0 )
                    {
                        title   = titleString;
                    }
                }
            }
            else
            {
                consumeCharacterContent();
            }

            super.endElement( uri , localName , qName );
        }
    }

    /** A term element. */

    public class TermElementHandler extends GenericElementHandler
    {
        @Override
        public void startElement
        (
            String uri ,
            String localName ,
            String qName ,
            Attributes attributes
        )
        {
            super.startElement( uri , localName , qName , attributes );

            consumeCharacterContent();
        }

        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
                                //  See if we at the keywords
                                //  element inside the profileDesc.

            if  (   !elementHandlers.get( "profileDesc" ).insideElement() ||
                    !elementHandlers.get( "textClass" ).insideElement() ||
                    !elementHandlers.get( "keywords" ).insideElement()
                )
            {
                super.endElement( uri , localName , qName );
                return;
            }
                                //  Get the term string.

            String termString   = consumeCharacterContent();

            termString  = termString.replaceAll( "\\s+" , " " ).trim();

                                //  Add non-empty term string to keywords.

            if ( termString.length() > 0 )
            {
                keywords.add( termString );
            }

            super.endElement( uri , localName , qName );
        }
    }

    /** A date element. */

    public class DateElementHandler extends GenericElementHandler
    {
        protected String dateType;

        @Override
        public void startElement
        (
            String uri ,
            String localName ,
            String qName ,
            Attributes attributes
        )
        {
            super.startElement( uri , localName , qName , attributes );

                                //  Extract date type if given.
                                //  Set date type to empty string otherwise.
            dateType    = "";

            if ( attributes.getValue( "type" ) != null )
            {
                dateType = attributes.getValue( "type" );
            }
        }

        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
                                //  Get date string.

            String dateString   =
                cleanDateString( consumeCharacterContent() );

                                //  If we have a date type, see if
                                //  it is a creation_date .

            if ( dateType.length() > 0 )
            {
                if  (   dateType.equals( "creation_date" ) &&
                        ( creationDate == Integer.MIN_VALUE )
                    )
                {
                    try
                    {
                        creationDate    = Integer.parseInt( dateString );
                    }
                    catch ( Exception e )
                    {
                    }
                }

                super.endElement( uri , localName , qName );
                return;
            }
                                //  Try creation date in profileDesc.

            if  (   ( creationDate == Integer.MIN_VALUE ) &&
                    elementHandlers.get( "profileDesc" ).insideElement() &&
                    elementHandlers.get( "creation" ).insideElement()
                )
            {
                try
                {
                    creationDate    = Integer.parseInt( dateString );
                }
                catch ( Exception e )
                {
                }

                super.endElement( uri , localName , qName );
                return;
            }
                                //  Not creation date.
                                //  See if it is a publication date.

            if  (   ( pubDate != Integer.MIN_VALUE ) ||
                    !elementHandlers.get( "sourceDesc" ).insideElement() ||
                    !elementHandlers.get( "biblFull" ).insideElement() ||
                    !elementHandlers.get( "publicationStmt" ).insideElement()
                )
            {
                super.endElement( uri , localName , qName );
                return;
            }
                                //  Convert to integer publication date.
            pubDate = 0;

            try
            {
                pubDate = Integer.parseInt( dateString );
            }
            catch ( Exception e )
            {
            }

            super.endElement( uri , localName , qName );
        }
    }

    /** A circulationYear element. */

    public class CirculationYearElementHandler
        extends GenericElementHandler
    {
        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
            if ( !elementHandlers.get( "monkHeader" ).insideElement() )
            {
                super.endElement( uri , localName , qName );
                return;
            }
                                //  Get circulation year string.

            String dateString   =
                cleanDateString( consumeCharacterContent() );

                                //  Convert to integer publication date.
            pubDate = 0;

            try
            {
                pubDate = Integer.parseInt( dateString );
            }
            catch ( Exception e )
            {
            }

            super.endElement( uri , localName , qName );
        }
    }

    /** A creationYear element. */

    public class CreationYearElementHandler
        extends GenericElementHandler
    {
        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
            if ( !elementHandlers.get( "epHeader" ).insideElement() )
            {
                super.endElement( uri , localName , qName );
                return;
            }
                                //  Get creation year string.

            String dateString   =
                cleanDateString( consumeCharacterContent() );

                                //  Convert to integer publication date.
            pubDate = 0;

            try
            {
                pubDate = Integer.parseInt( dateString );
            }
            catch ( Exception e )
            {
            }

            super.endElement( uri , localName , qName );
        }
    }

    /** Clean date string.
     *
     *  @param  dateString  Date string to clean.
     *
     *  @return     Cleaned date string.
     */

    public String cleanDateString( String dateString )
    {
                                //  Clean date.

        String result   = "";
                                //  Throw away non-digits in date.

        StringBuilder sb    = new StringBuilder();

        for ( int j = 0 ; j < dateString.length() ; j++ )
        {
            char ch = dateString.charAt( j );

            switch ( ch )
            {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    sb.append( ch );
                    break;

                default :
                    sb.append( ' ' );
            }
        }
                                //  Normalize whitespace.

        result = sb.toString().replaceAll( "\\s+" , " " ).trim();

                                //  Assume to date is what appears
                                //  before the first blank.

        int i   = result.indexOf( " " );

        if ( i >= 0 )
        {
            result = result.substring( 0 , i );
        }

        return result;
    }

    /** Clean person string.
     *
     *  @param  personString    Person string to clean.
     *
     *  @return     Cleaned person string.
     */

    public String cleanPersonString( String personString )
    {
        String result   = "";

        if ( personString != null )
        {
                                //  Remove some cruft.

            result  = personString.replace( "^([^,]*,[^,]*),?.*$", "$1" );

                                //  Normalize whitespace.

            result = result.replaceAll( "\\s+" , " " ).trim();
        }

        return result;
    }

    /** A word element. */

    public class WElementHandler extends WordHandlerBase
    {
        @Override
        public void startElement
        (
            String uri ,
            String localName ,
            String qName ,
            Attributes attributes
        )
        {
            super.startElement( uri , localName , qName , attributes );

                                //  Increment count of <w> elements.
            wordCount++;
                                //  Ignore words that are not inside
                                //  a <text> element.

            ElementHandler textElement = elementHandlers.get( "text" );

            if ( !textElement.insideElement() )
            {
                return;
            }
                                //  Ignore words that are inside an ignorable
                                //  <div> element.

            DivElementHandler divElementHandler =
                (DivElementHandler)elementHandlers.get( "div" );

            if ( divElementHandler != null )
            {
                if ( !divElementHandler.indexDivContents() )
                {
                    return;
                }
            }
                                //  Set the word attribute values.

            setWordFields( attributes , false );

                                //  Add normalized whitespace and
                                //  punctuation.
            propPunct.addValue
            (
                StringUtil.normalizeWhitespace
                (
                    consumeCharacterContent()
                )
            );
        }

        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
            super.endElement( uri , localName , qName );

                                //  Ignore words that are not inside
                                //  a <text> element.

            ElementHandler textElement = elementHandlers.get( "text" );

            if ( !textElement.insideElement() )
            {
                return;
            }
                                //  Ignore words that are inside an ignorable
                                //  <div> element.

            DivElementHandler divElementHandler =
                (DivElementHandler)elementHandlers.get( "div" );

            if ( divElementHandler != null )
            {
                if ( !divElementHandler.indexDivContents() )
                {
                    return;
                }
            }
                                //  Add word text to main content.

            if ( wordText.length() > 0 )
            {
                consumeCharacterContent();
            }
            else
            {
                wordText    = consumeCharacterContent();
            }

            propMain.addValue( wordText );

                                //  If the orig= attribute was not specified,
                                //  set its value to the word text.

            if ( ( origText == null ) || origText.equals( "" ) )
            {
                origText    = wordText;
            }

            propOrig.addValue( origText );

                                //  Add reversed word text if requested.

            if ( indexReversedWord )
            {
                propRevWord.addValue( reverseString( wordText ) );
            }
                                //  Set gap marker values.
            if ( indexGaps )
            {
                                //  See if word text contains a
                                //  blackdot character.  Set gap
                                //  attribute to "c" if so.

                if ( wordText.indexOf( gapCharMarker ) > 0 )
                {
                    propGap.addValue( "c" );
                }
                                //  See if word text contains a
                                //  missing punctuation character.
                                //  Set gapp attribute to "y" if so, else
                                //  set gapp attribute to "n".

                else if ( wordText.indexOf( gapPuncMarker ) > 0 )
                {
                    propGap.addValue( "p" );
                }
                                //  See if word text contains a
                                //  missing span character.
                                //  Set gap attribute to "s" if so.

                else if ( wordText.indexOf( gapSpanMarker ) > 0 )
                {
                    propGap.addValue( "s" );
                }
                                //  See if word text contains a
                                //  missing word character.
                                //  Set gap attribute to "w" if so.

                else if ( wordText.indexOf( gapWordMarker ) > 0 )
                {
                    propGap.addValue( "w" );
                }
                                //  No gap?  Set gap attribute to "n".
                else
                {
                    propGap.addValue( "n" );
                }
            }
                                //  Add standard spelling from reg=
                                //  attribute, <reg> descendant element,
                                //  or just use the word text.

            if ( regText.length() == 0 )
            {
                regText = wordText;
            }

            propReg.addValue( regText );

                                //  Add reversed standard spelling if requested.

            if ( indexReversedStandardSpelling )
            {
                propRevReg.addValue( reverseString( regText ) );
            }
        }
    }

    /** See if end of sentence in item attributes.
     *
     *  @param  attributes  The w or pc element attributes.
     *
     *  @return     true if item is end of sentence, false otherwise.
     */

    public boolean isEndOfSentence( Attributes attributes )
    {
        boolean result  = false;

        String unit = attributes.getValue( atrUnit );

        if ( unit != null )
        {
            unit    = unit.toLowerCase();
        }
        else
        {
            unit    = "";
        }

        result  = unit.equals( "sentence" );

        if ( !result )
        {
            String eos      = "n";
            String eosAttr  = attributes.getValue( atrEOS );

            if ( eosAttr != null )
            {
                if ( eosAttr.trim().equals( "1" ) )
                {
                    eos = "y";
                }
            }
            else if ( unit.equals( "sentence" ) )
            {
                eos = "y";
            }

            result  = eos.equals( "y" );
        }

        return result;
    }

    /** Reverse a string.
     *
     *  @param  s   String to reverse.
     *
     *  @return     Reversed string.
     */

    public String reverseString( String s )
    {
        StringBuilder sb = new StringBuilder();

        sb.append( s );

        sb = sb.reverse();

        return sb.toString();
    }

    /** Create handlers for pc and w for processing punctuation. */

    GenericElementHandler genericElementHandler = new GenericElementHandler();
    WElementHandler wElementHandler = new WElementHandler();

    /** A punctuation element. */

    public class PcElementHandler extends WordHandlerBase
    {
        boolean isEOS;

        @Override
        public void startElement
        (
            String uri ,
            String localName ,
            String qName ,
            Attributes attributes
        )
        {
//          super.startElement( uri , localName , qName , attributes );

                                //  Increment count of <pc> elements.
            pcCount++;
                                //  Assume not end of sentence.

            isEOS   = false;
                                //  If we're only processing punctuation
                                //  that ends a sentence, check if this
                                //  entry ends a sentence.  Otherwise
                                //  don't index it.

            switch ( indexPunctuation )
            {
                case NONE   :
                    genericElementHandler.startElement( uri, localName, qName, attributes );
                    break;

                case PARTIAL:
                    isEOS   = isEndOfSentence( attributes );

                    if ( isEOS )
                    {
                        wElementHandler.startElement( uri, localName, qName, attributes );
                    }
                    else
                    {
                        genericElementHandler.startElement( uri, localName, qName, attributes );
                    }
                    break;

                default :
                    wElementHandler.startElement( uri, localName, qName, attributes );
                    break;
            }
        }

        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
//          super.endElement( uri , localName , qName );

            switch ( indexPunctuation )
            {
                case NONE   :
                    genericElementHandler.endElement( uri, localName, qName );
                    break;

                case PARTIAL:
                    if ( isEOS )
                    {
                        wElementHandler.endElement( uri, localName, qName );
                    }
                    else
                    {
                        genericElementHandler.endElement( uri, localName, qName );
                    }
                    break;

                default :
                    wElementHandler.endElement( uri, localName, qName );
                    break;
            }
        }
    }

    /** An orig element descending from a w element */

    public class OrigElementHandler extends ContentCapturingHandler
    {
        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
            if  (   !elementHandlers.get( "w" ).insideElement() ||
                    !elementHandlers.get( "choice" ).insideElement()
                )
            {
                super.endElement( uri , localName , qName );
                return;
            }

            wordText    = consumeCharacterContent();

            super.endElement( uri , localName , qName );
        }
    }

    /** A reg element descending from a w element */

    public class RegElementHandler extends ContentCapturingHandler
    {
        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
            if  (   !elementHandlers.get( "w" ).insideElement() ||
                    !elementHandlers.get( "choice" ).insideElement()
                )
            {
                super.endElement( uri , localName , qName );
                return;
            }

            regText = consumeCharacterContent();

            super.endElement( uri , localName , qName );
        }
    }

    /** A proofreader element descending from a special header element */

    public class ProofReaderElementHandler extends ContentCapturingHandler
    {
        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
            boolean inEpHeader  =
                useEpHeader &&
                elementHandlers.get( "epHeader" ).insideElement();

            boolean inNuHeader      =
                useNuHeader &&
                elementHandlers.get( "nuHeader" ).insideElement();

            if ( !( inEpHeader || inNuHeader ) )
            {
                super.endElement( uri , localName , qName );
                return;
            }

            proofReaders.add
            (
                cleanPersonString( consumeCharacterContent() )
            );

            super.endElement( uri , localName , qName );
        }
    }

    /** A curator element descending from a special header element */

    public class CuratorElementHandler extends ContentCapturingHandler
    {
        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
            boolean inEpHeader  =
                useEpHeader &&
                elementHandlers.get( "epHeader" ).insideElement();

            boolean inNuHeader      =
                useNuHeader &&
                elementHandlers.get( "nuHeader" ).insideElement();

            if ( !( inEpHeader || inNuHeader ) )
            {
                super.endElement( uri , localName , qName );
                return;
            }

            curators.add
            (
                cleanPersonString( consumeCharacterContent() )
            );

            super.endElement( uri , localName , qName );
        }
    }

    /** A corpus element descending from a special header element */

    public class CorpusElementHandler extends ContentCapturingHandler
    {
       @Override
        public void startElement
        (
            String uri ,
            String localName ,
            String qName ,
            Attributes attributes
        )
        {
            super.startElement( uri , localName , qName , attributes );

            consumeCharacterContent();
        }

        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
            boolean inEpHeader  =
                useEpHeader &&
                elementHandlers.get( "epHeader" ).insideElement();

            boolean inNuHeader      =
                useNuHeader &&
                elementHandlers.get( "nuHeader" ).insideElement();

            if ( !( inEpHeader || inNuHeader ) )
            {
                super.endElement( uri , localName , qName );
                return;
            }
                                //  Kludge for EarlyPrint.

            String corpusName   = consumeCharacterContent().trim();

            if ( corpusName.equals( "shc" ) )
            {
                corpusName  = "drama";
            }

            corpora.add( corpusName );

            super.endElement( uri , localName , qName );
        }
    }

    /** A document identifier element descending from a special header element */

    public class DocumentIdentifierElementHandler
        extends ContentCapturingHandler
    {
        @Override
        public void startElement
        (
            String uri ,
            String localName ,
            String qName ,
            Attributes attributes
        )
        {
            super.startElement( uri , localName , qName , attributes );

            consumeCharacterContent();
        }

        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
            boolean inEpHeader  =
                useEpHeader &&
                elementHandlers.get( "epHeader" ).insideElement();

            boolean inNuHeader      =
                useNuHeader &&
                elementHandlers.get( "nuHeader" ).insideElement();

            if ( !( inEpHeader || inNuHeader ) )
            {
                super.endElement( uri , localName , qName );
                return;
            }

            documentIdentifiers.add( consumeCharacterContent().trim() );

            super.endElement( uri , localName , qName );
        }
    }

    /** A publicationYear element. */

    public class PublicationYearElementHandler
        extends GenericElementHandler
    {
        @Override
        public void endElement
        (
            String uri ,
            String localName ,
            String qName
        )
        {
            if ( !elementHandlers.get( "epHeader" ).insideElement() )
            {
                super.endElement( uri , localName , qName );
                return;
            }
                                //  Get creation year string.

            String dateString   =
                cleanDateString( consumeCharacterContent() );

                                //  Convert to integer publication date.

            publicationYear = Integer.MIN_VALUE;

            try
            {
                publicationYear = Integer.parseInt( dateString );
            }
            catch ( Exception e )
            {
                publicationYear = Integer.MIN_VALUE;
            }

            super.endElement( uri , localName , qName );
        }
    }
}

//#include "standard.lic"
